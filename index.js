const Client = require('./client');

module.exports = ({host, port, endpoint, user, password}) => {
	if (!host) {
		host = '127.0.0.1';
	}

	if (!port) {
		port = 80
	}

	if (!endpoint) {
		endpoint = '/RPC2';
	}

	if(user && !password) {
		throw new Error('"password" is missing');
	}

	return new Client(host, port, endpoint, user, password);
};
const xmlrpc = require('xmlrpc');
const _path = require('path');
const fs = require('fs');

class Client {
  constructor(host, port, endpoint, user, password) {
    this.client = xmlrpc.createClient({
      host,
      port,
      path: endpoint || '/RC2',
      basic_auth: {
        user: user || null,
        pass: password || null,
      },
      encoding: 'UTF-8'
    });
  }

  async getVersion() {
		return `rTorrent - v${(await this.call('system.client_version'))}`;
	}

  async get(details, withFiles) {
    const hashList = await this.call('download_list', []);
    if (!details) {
      return hashList;
    }

    const arrayReturn = [];
    for (let i = 0; i < hashList.length; i++) {
      arrayReturn.push(await this.getOne(hashList[i], withFiles));
    }

    return arrayReturn;
  }

  async getOne(hash, withFiles) {
    /** Check data already downloaded */
    const completedByte = await this.call('d.completed_bytes', [hash]);

    /** Total size of torrent */
    const sizeBytes = await this.call('d.size_bytes', [hash]);

    /** Torrent completed or not */
    const complete = await this.call('d.complete', [hash]);

    /** Check rate downloaded (speed download) */
    const down_rate = await this.call('d.down.rate', [hash]);

    /** Check size uploaded */
    const upTotal = await this.call('d.up.total', [hash]);

    /** Check if torrent is playing or not */
    const isActive = await this.call('d.is_active', [hash]);

    /** Get name */
    const name = await this.call('d.name', [hash]);

    /** Creation date **/
    const createdAt = await this.call('d.creation_date', [hash]);

    /** Get ration up/down */
    const ratio = await this.call('d.ratio', [hash]);

    /** Get free space */
    const free_space = await this.call('d.free_diskspace', [hash]);

    /** Get trackers list */
    const trackers = await this.call('t.multicall', [hash, '', 't.url=']);

    /** Get session torrent file */
    const torrentFile = await this.call('d.loaded_file', [hash]);

    const data = {
      hash: hash,
      name: name,
      torrentFile,
      active: isActive === '1',
      downloaded: Number(completedByte),
      uploaded: Number(upTotal),
      length: Number(sizeBytes),
      ratio: ratio / 1000,
      complete,
      createdAt: (new Date(createdAt*1000)).toJSON(),
      free_space,
      down_rate,
      trackers: trackers.map(tracker => tracker.shift()),
      extra: {
        ratio: ratio / 1000,
      },
    };

    if(withFiles) {
      data.files = await this.getFiles(hash);
    }

    return data;
  }

  async getFiles(hash) {
    return (await this.call('f.multicall', [hash, "", "f.path=", "f.size_bytes=", "f.completed_chunks=", "f.size_chunks="]))
      .map((f) => ({
        name: f[0],
        length: parseInt(f[1]),
        isCompleted: f[2] === f[3],
      }));
  }

  async play(hash) {
    return this.call('d.resume', [hash]);
  }

  async pause(hash) {
    return this.call('d.pause', [hash]);
  }

  async remove(hash) {
    return this.call('d.erase', [hash]);
  }

  async createFromFile(file) {
    const content = fs.readFileSync(file);
    return this.call('load.raw_start', ["", content]);
  }

  async createFromBuffer(buffer) {
    return this.call('load.raw_start', ["", buffer]);
  }

  call(method, params) {
    return new Promise((resolve, reject) => {
      this.client.methodCall(method, params, (error, result) => {
        if (error) {
          reject(error);
        } else {
          resolve(result);
        }
      });
    });
  }
}

module.exports = Client;
